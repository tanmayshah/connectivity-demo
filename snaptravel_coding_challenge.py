''' 
-------------------------------------------------------------------------------------------

Problem     : SnapTravel Technical Take-Home Challenge
Description : https://gitlab.com/snaptravel/connectivity-demo/
Language    : Python 3
Code Author : Tanmay 

Package Requirements for program execution:
-> Requests        (https://2.python-requests.org/en/master/)
-> XML ElementTree (https://docs.python.org/2/library/xml.etree.elementtree.html)
-> SQLite3         (https://docs.python.org/2/library/sqlite3.html)
-> PPrint          (https://docs.python.org/2/library/pprint.html)

------------------------------------------------------------------------------------------- 
'''
import xml.etree.ElementTree as ET
import pprint as pp
import requests
import sqlite3

# Fetch input from command line and return 3-tuple (assuming valid input is entered)  
def fetch_input():
	city_str = input("City: ").lower()
	checkin_str = input("Check-In: ").lower()
	checkout_str = input("Check-Out: ").lower()

	return (city_str, checkin_str, checkout_str)


# Make HTTP POST request and return responses
def make_requests(city_name, checkin_date, checkout_date):
	# JSON Request 
	api_1_url = 'https://experimentation.getsnaptravel.com/interview/hotels'
	payload = {'city': city_name, 'checkin': checkin_date, 'checkout': checkout_date, 'provider': 'snaptravel'}
	response = requests.post(api_1_url, json=payload)
	json_response = response.json()

	# XML Request
	api_2_url = 'https://experimentation.getsnaptravel.com/interview/legacy_hotels'
	xml = '''<?xml version="1.0" encoding="UTF-8"?>
			 <root>
				<checkin>checkin_string_input</checkin>
				<checkout>checkout_string_input</checkout>
				<city>city_string_input</city>
				<provider>snaptravel</provider>
			 </root>
		  '''
	headers = {'Content-Type': 'application/xml'}
	response = requests.post(api_2_url, data=xml, headers=headers)
	xml_response = response.text

	# Return responses
	return json_response, xml_response


# Finds and returns a list of common hotels
def common_hotels(json_data, xml_data):
	xml_data = xml_data[len('<?xml version="1.0"?>'):]
	xml_tree = ET.fromstring(xml_data)
	common_hotels_list = []
	
	for element in xml_tree.findall('element'):
		identifier = int(element.find('id').text)
		retail_price = float(element.find('price').text)

		for json_hotel in json_data:
			if json_hotel['id'] == identifier:
				json_hotel['price'] = {'snaptravel': json_hotel['price'], 'retail': retail_price}
				common_hotels_list.append(json_hotel)
				break

	return common_hotels_list


# Main function that begins program execution
def main():

	# get inputs
	city, checkin, checkout = fetch_input()

	# produce list of common hotels from server responses
	json_response, xml_response = make_requests(city,checkin,checkout)
	common_hotels_list = common_hotels(json_response['hotels'],xml_response)
	
	# setup db connection 
	conn = sqlite3.connect('snaptravel.db')
	c = conn.cursor()

	# create db table storing list of common hotels
	create_query = ''' 
					CREATE TABLE IF NOT EXISTS hotels(
						id INTEGER,
						hotel_name TEXT,
						address TEXT,
						amenities TEXT,
						image_url TEXT,
						num_reviews INTEGER,
						num_stars INTEGER,
						retail_price REAL,
						snaptravel_price REAL)
				   '''
	c.execute(create_query)

	# insert info. about common hotels into db table
	for hotel in common_hotels_list:
		
		# modify amenties from list to string for db storage
		amenities_str = ''
		for amenity in hotel['amenities']:
			amenities_str += amenity+', '
		amenities_str = amenities_str[0:-2] if len(amenities_str) >= 2 else amenities_str
		
		# insert command
		c.execute('''INSERT INTO hotels VALUES (
						:id, 
						:hotel_name, 
						:address, 
						:amenities,
						:image_url,
						:num_reviews, 
						:num_stars, 
						:retail_price, 
						:snaptravel_price)''', 

						{'id': hotel['id'], 
						 'hotel_name': hotel['hotel_name'],
						 'address': hotel['address'], 
						 'amenities': amenities_str,
						 'image_url': hotel['image_url'], 
						 'num_reviews': hotel['num_reviews'], 
						 'num_stars': hotel['num_stars'], 
						 'retail_price': hotel['price']['retail'], 
						 'snaptravel_price': hotel['price']['snaptravel']})

		conn.commit()

	# read db table (uncomment for debbugging)
	#c.execute('SELECT * FROM hotels')
	#pp.pprint(c.fetchall())

	# close db connection
	conn.close()


if __name__== "__main__":
	main()
