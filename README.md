# How To Run Code
Review instructions below to understand how to run and test code for this challenge

### Pre-requisites
All code is writted in Python 3. The following packages are necessary for successful execution:
1) Requests        (https://2.python-requests.org/en/master/)
2) XML ElementTree (https://docs.python.org/2/library/xml.etree.elementtree.html)
3) SQLite3         (https://docs.python.org/2/library/sqlite3.html)
4) PPrint          (https://docs.python.org/2/library/pprint.html)

You may install missing packages using the **pip** command
```console
pip install <package name>
```

### Running Program in Terminal
From the terminal, run the following command to begin program execution
```console
python snaptravel_coding_challenge.py
```
Next, the program will generate prompts asking for **valid** program input. Sample inputs can be
```
City: Tokyo
Check-In: Feb 10, 2019
Check-Out: Feb 12, 2019
```

If no errors are generated after entering inputs, this suggests that program ran successfully and the API responses were stored in  *snaptravel.db* file 